﻿using Assignment3.DTOs.MovieDTOs;
using Assignment3.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Assignment3.Interfaces
{
    public interface IMovieRepository
    {


        /// <summary>
        /// Abstract method for get all movies.
        /// </summary>
        /// <returns>A list of movies</returns>
        Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMoviesAsync();
        
        /// <summary>
        /// Abstract method for get a movie by Id.
        /// </summary>
        /// <param name="id">The id of the movie</param>
        /// <returns>The movieDto object.</returns>
        Task<ActionResult<MovieReadDTO>> GetMovieAsync(int id);


        /// <summary>
        /// Abstract method to get all characters in a movie.
        /// </summary>
        /// <param name="id">Id of the movie.</param>
        /// <returns></returns>
        Task<ActionResult<IEnumerable<Character>>> GetAllCharactersInMovieAsync(int id);


        /// <summary>
        /// Abstract method for create a movie.
        /// </summary>
        /// <param name="movieDto">MovieDto object.</param>
        /// <returns>The new movieDto object</returns>
        Task<ActionResult<MovieReadDTO>> PostMovieAsync(MovieCreateDTO movieDto);

        /// <summary>
        /// Abstract method to update a movie.
        /// </summary>
        /// <param name="id">The id of the movie</param>
        /// <param name="movieDto">MovieDto object</param>
        /// <returns>MovieDto object.</returns>
        Task<ActionResult<MovieUpdateDTO>> PutMovieAsync(int id, MovieUpdateDTO movieDto);

       /// <summary>
       /// Abstract method to delete a movie.
       /// </summary>
       /// <param name="id">The id of the movie</param>
       /// <returns></returns>
        Task<ActionResult<MovieDeleteDTO>> DeleteMovieAsync(int id);


        /// <summary>
        /// Abstract method to update movies in a franchise.
        /// </summary>
        /// <param name="id">Id of the franchise</param>
        /// <param name="movieId">Id of the movie</param>
        /// <param name="movieDto">movieDto</param>
        /// <returns>A movieDto object.</returns>
        Task<ActionResult<MovieUpdateDTO>> PutMoviesInFranchiseAsync(int id, int movieId, MovieUpdateDTO movieDto);
    }
}
