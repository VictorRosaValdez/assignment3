﻿using Assignment3.DTOs.CharacterDTOs;
using Assignment3.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Assignment3.Interfaces
{
    public interface ICharacterRepository
    {
        /// <summary>
        /// Abstract method for get all characters.
        /// </summary>
        /// <returns>A list of charactersDto.</returns>
        Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharactersAsync();

        /// <summary>
        /// Abstract method for get a character by Id.
        /// </summary>
        /// <param name="id">The id of the character</param>
        /// <returns>The characterDto object.</returns>
        Task<ActionResult<CharacterReadDTO>> GetCharacterAsync(int id);

        

        /// <summary>
        /// Abstract method for create a character.
        /// </summary>
        /// <param name="characterDto">CharacterDto object.</param>
        /// <returns>The new characterDto object</returns>
        Task<ActionResult<CharacterReadDTO>> PostCharacterAsync(CharacterCreateDTO characterDto);

        /// <summary>
        /// Abstract method to update a character.
        /// </summary>
        /// <param name="id">The id of the character</param>
        /// <param name="characterDto">CharacterDto object</param>
        /// <returns>CharacterDto object.</returns>
        Task<ActionResult<CharacterUpdateDTO>> PutCharacterAsync(int id, CharacterUpdateDTO characterDto);

        /// <summary>
        /// Abstract method to delete a character.
        /// </summary>
        /// <param name="id">The id of the character</param>
        /// <returns></returns>
        Task<ActionResult<CharacterDeleteDTO>> DeleteCharacterAsync(int id);


        /// <summary>
        /// Abstract method to update characters in movie.
        /// </summary>
        /// <param name="id">Id of the movie</param>
        /// <param name="characterId">Id of the character</param>
        /// <param name="characterDto">characterDto</param>
        /// <returns>A characterDto object.</returns>
        Task<ActionResult<CharacterUpdateDTO>> PutCharactersInMovieAsync(int id, int characterId, CharacterUpdateDTO characterDto);

    }
}
