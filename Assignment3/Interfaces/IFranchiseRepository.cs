﻿using Assignment3.DTOs.FranchiseDTOs;
using Assignment3.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Assignment3.Interfaces
{
    public interface IFranchiseRepository
    {
        /// <summary>
        /// Abstract method for get all Frnachises.
        /// </summary>
        /// <returns>A list of frnachises</returns>
        Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchisesAsync();

        /// <summary>
        /// Abstract method for get a franchise by Id.
        /// </summary>
        /// <param name="id">The id of the franchise</param>
        /// <returns>The franchiseDto object.</returns>
        Task<ActionResult<FranchiseReadDTO>> GetFranchiseAsync(int id);

        /// <summary>
        /// Abstract method to get all movie in a franchise.
        /// </summary>
        /// <param name="id">Id of franchise.</param>
        /// <returns>List of movie objects.</returns>
        Task<ActionResult<Franchise>> GetAllMoviesInFranchiseAsync(int id);

        /// <summary>
        /// Abstract method to get all characters in a franchise.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ActionResult<IEnumerable<Character>>> GetAllCharactersInFranchiseAsync(int id);

        /// <summary>
        /// Abstract method for create a franchise.
        /// </summary>
        /// <param name="franchiseDto">FranchiseDto object.</param>
        /// <returns>The new franchiseDto object</returns>
        Task<ActionResult<FranchiseReadDTO>> PostFranchiseAsync(FranchiseCreateDTO franchiseDto);

        /// <summary>
        /// Abstract method to update a franchise.
        /// </summary>
        /// <param name="id">The id of the franchise</param>
        /// <param name="franchiseDto">FranchiseDto object</param>
        /// <returns>The franchiseDto object.</returns>
        Task<ActionResult<FranchiseUpdateDTO>> PutFranchiseAsync(int id, FranchiseUpdateDTO franchiseDto);

        /// <summary>
        /// Abstract method to delete a franchise.
        /// </summary>
        /// <param name="id">The id of the franchise</param>
        /// <returns></returns>
        Task<ActionResult<FranchiseDeleteDTO>> DeleteFranchiseAsync(int id);



    }
}
