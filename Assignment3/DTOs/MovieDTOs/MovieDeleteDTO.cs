﻿using System.ComponentModel.DataAnnotations;

namespace Assignment3.DTOs.MovieDTOs
{
    public class MovieDeleteDTO
    {
        // Prperties of the DTO.
        public int MovieId { get; set; }
        [MaxLength(50)] public string Title { get; set; }

    }
}
