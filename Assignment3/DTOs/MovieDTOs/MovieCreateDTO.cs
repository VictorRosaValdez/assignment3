﻿using System.ComponentModel.DataAnnotations;

namespace Assignment3.DTOs.MovieDTOs
{
    public class MovieCreateDTO
    {
        // Prperties of the DTO.
        [MaxLength(50)] public string Title { get; set; }

        [MaxLength(50)] public string Genre { get; set; }

        public int Year { get; set; }

        [MaxLength(50)] public string Director { get; set; }

        [MaxLength(300)] public string Picture { get; set; }

        [MaxLength(300)] public string Trailer { get; set; }

        // Navigation Property (Foreign key)
        public int FranchiseId { get; set; }
    }
}
