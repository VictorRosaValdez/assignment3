﻿using System.ComponentModel.DataAnnotations;

namespace Assignment3.DTOs.MovieDTOs
{
    public class MovieReadDTO
    {
        // Prperties of the DTO.
        public int MovieId { get; set; }
        [MaxLength(50)] public string Title { get; set; }

        [MaxLength(50)] public string Genre { get; set; }

        public int Year { get; set; }

        [MaxLength(50)] public string Director { get; set; }

        [MaxLength(300)] public string Picture { get; set; }

        [MaxLength(300)] public string Trailer { get; set; }

     
    }
}
