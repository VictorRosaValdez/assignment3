﻿using System.ComponentModel.DataAnnotations;

namespace Assignment3.DTOs.FranchiseDTOs
{
    public class FranchiseReadDTO
    {
        // Prperties of the DTO.
        public int FranchiseId { get; set; }
        [MaxLength(50)] public string Name { get; set; }
        [MaxLength(300)] public string Description { get; set; }

       
    }
}
