﻿using Assignment3.DTOs.FranchiseDTOs;
using Assignment3.Models;
using System.ComponentModel.DataAnnotations;

namespace Assignment3.DTOs.FranchiseDTOs
{
    public class FranchiseCreateDTO
    {
        // Prperties of the DTO.
        [MaxLength(50)] public string Name { get; set; }
        [MaxLength(300)] public string Description { get; set; }

    }
}
