﻿using System.ComponentModel.DataAnnotations;

namespace Assignment3.DTOs.FranchiseDTOs
{
    public class FranchiseDeleteDTO
    {
        // Prperties of the DTO.
        public int FranchiseId { get; set; }
        [MaxLength(50)] public string Name { get; set; }
    }
}
