﻿using System.ComponentModel.DataAnnotations;

namespace Assignment3.DTOs.CharacterDTOs
{
    public class CharacterReadDTO
    {
        // Prperties of the DTO.
        public int CharacterId { get; set; }
        [MaxLength(50)] public string FullName { get; set; }
        [MaxLength(50)] public string? Alias { get; set; }

        [MaxLength(50)] public string Gender { get; set; }

        [MaxLength(300)] public string Picture { get; set; }
    }
}
