﻿using System.ComponentModel.DataAnnotations;

namespace Assignment3.DTOs.CharacterDTOs
{
  
    public class CharacterCreateDTO
    {
        // Prperties of the DTO.
        [MaxLength(50)] public string FullName { get; set; }
        [MaxLength(50)] public string? Alias { get; set; }

        [MaxLength(50)] public string Gender { get; set; }

        [MaxLength(300)] public string Picture { get; set; }
    }
}
