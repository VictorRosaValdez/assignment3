﻿using System.ComponentModel.DataAnnotations;

namespace Assignment3.DTOs.CharacterDTOs
{
    public class CharacterUpdateDTO
    {
        // Prperties of the DTO.

        public int CharacterId { get; set; }
        [MaxLength(50)] public string FullName { get; set; }
    }
}
