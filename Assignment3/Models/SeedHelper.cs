﻿using System.Collections.Generic;

namespace Assignment3.Models
{
    public class SeedHelper
    {
        /// <summary>
        /// Seeded data for movies.
        /// </summary>
        /// <returns>List of movies.</returns>
        public static IEnumerable<Movie> GetMovieSeeds()
        {
            var movie = new List<Movie>()
            {
                new Movie()
                {
                 MovieId = 1,
                 Title = "Avengers",
                 Genre = "Fantasy film",
                 Year = 2012,
                 FranchiseId = 1,
                 Director = "Anthony Russo",
                 Picture = "https://en.wikipedia.org/wiki/The_Avengers_(2012_film)#/media/File:The_Avengers_(2012_film)_poster.jpg",
                 Trailer = "https://www.youtube.com/watch?v=eOrNdBpGMv8"

                },

                new Movie()
                {
                 MovieId = 2,
                 Title = "Avengers: Endgame",
                 Genre = "Fantasy film",
                 Year = 2019,
                 FranchiseId = 1,
                 Director = "Anthony Russo",
                 Picture = "https://en.wikipedia.org/wiki/Avengers:_Endgame#/media/File:Avengers_Endgame_poster.jpg",
                 Trailer = "https://www.youtube.com/watch?v=TcMBFSGVi1c"

                },

                  new Movie()
                {
                 MovieId = 3,
                 Title = "Gladiator",
                 Genre = "Historical drama",
                 Year = 2000,
                 FranchiseId = 2,
                 Director = "Ridley Scott",
                 Picture = "https://en.wikipedia.org/wiki/Gladiator_(2000_film)#/media/File:Gladiator_(2000_film_poster).png",
                 Trailer = "https://www.youtube.com/watch?v=owK1qxDselE"

                },

                    new Movie()
                {
                 MovieId = 4,
                 Title = "Avatar",
                 Genre = "Science fiction film",
                 Year = 2000,
                 FranchiseId = 3,
                 Director = "James Cameron",
                 Picture = "https://en.wikipedia.org/wiki/Avatar_(2009_film)#/media/File:Avatar_(2009_film)_poster.jpg",
                 Trailer = "https://www.youtube.com/watch?v=a8Gx8wiNbs8"

                }


            };

            return movie;
        }

        /// <summary>
        /// Seeded data for franchises.
        /// </summary>
        /// <returns>List of franchises.</returns>
        public static IEnumerable<Franchise> GetFranchiseSeeds()
        {
            var franchise = new List<Franchise>()
            {
                new Franchise()
                {
                 FranchiseId = 1,
                 Name = "Marvel Studios",
                 Description = "Marvel Studios, LLC[4] (originally known as Marvel Films from 1993 to 1996)"
       


                },

                new Franchise()
                {
                 FranchiseId= 2,
                 Name = "DreamWorks Pictures",
                 Description = "DreamWorks Pictures (also known as DreamWorks SKG and formerly DreamWorks Studios"


                },

             new Franchise()
                {
                 FranchiseId= 3,
                 Name = "20th Century Studios",
                 Description = "20th Century Studios, Inc., (formerly known as 20th Century-Fox (1935–1985)"


                }


            };

            return franchise;
        }

        /// <summary>
        /// Seeded data for characters.
        /// </summary>
        /// <returns>List of characters.</returns>
        public static IEnumerable<Character> GetCharacterSeeds()
        {
            var character = new List<Character>()
            {
                new Character()
                {
                 CharacterId = 1,
                 FullName = "Robert Downey Jr.",
                 Alias = "Tony Stark / Iron Man",
                 Gender = "Male",
                 Picture = "https://en.wikipedia.org/wiki/Avengers:_Endgame#/media/File:Robert_Downey_Jr_2014_Comic_Con_(cropped).jpg"

                },

                 new Character()
                {
                 CharacterId = 2,
                 FullName = "Russell Crowe",
                 Alias = "General Maximus Decimus Meridius",
                 Gender = "Male",
                 Picture = "https://en.wikipedia.org/wiki/Russell_Crowe#/media/File:Russell_Crowe_(33994020424).jpg"

                },

                 new Character()
                {
                 CharacterId = 3,
                 FullName = "Sam Worthington",
                 Alias = "Jake Sully",
                 Gender = "Male",
                 Picture = "https://en.wikipedia.org/wiki/Sam_Worthington#/media/File:Sam_Worthington_2013.jpg"

                },

                new Character()
                {
                 CharacterId = 4,
                 FullName = "Scarlett Johansson",
                 Alias = "Natasha Romanoff / Black Widow",
                 Gender = "Female",
                 Picture = "https://en.wikipedia.org/wiki/Avengers:_Endgame#/media/File:Scarlett_Johansson_by_Gage_Skidmore_2_(cropped).jpg"

                }




            };

            return character;
        }

        /// <summary>
        /// Seeded data for MovieCharacters.
        /// </summary>
        /// <returns>List of MovieCharacters.</returns>
        public static IEnumerable<MovieCharacter> GetMovieCharacterSeeds() 
        {
            var movieCharacter = new List<MovieCharacter>() {

            new MovieCharacter()
            {
                MovieId = 1,
                CharacterId = 1,

            },

            new MovieCharacter()
            {
                MovieId = 2,
                CharacterId = 1,

            },

            new MovieCharacter()
            {
                MovieId = 1,
                CharacterId = 2,

            },

              new MovieCharacter()
            {
                MovieId = 2,
                CharacterId = 2,

            },



            };

            return movieCharacter;



        }

    }
}
