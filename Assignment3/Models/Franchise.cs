﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Assignment3.Models
{
    public class Franchise
    {
        // Properties of model
        public int FranchiseId { get; set; }
        [MaxLength(50)] public string Name { get; set; }
        [MaxLength(300)] public string Description { get; set; }

        // Navigation property
        public ICollection<Movie> Movie { get; set; }
        
    }
}
