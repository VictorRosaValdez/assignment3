﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Assignment3.Models
{
    public class Character
    {
        // Properties of model
        public int CharacterId { get; set; }
        [MaxLength(50)] public string FullName { get; set; }
        [MaxLength(50)]  public string? Alias { get; set; }

        [MaxLength(50)] public string Gender { get; set; }

        [MaxLength(300)] public string Picture { get; set; }

     

    }
}
