﻿namespace Assignment3.Models
{
    public class MovieCharacter
    {
        // Properties of model
        public int MovieId { get; set; }
        
        // Navigation property
        public Movie Movie { get; set; }

        public int CharacterId { get; set; }

        // Navigation property
        public Character Character { get; set; }
    }
}
