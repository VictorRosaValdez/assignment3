﻿using Assignment3.DTOs.MovieDTOs;
using Assignment3.Interfaces;
using Assignment3.Models;
using AutoMapper;
using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace Assignment3.Dal.Repositories
{
    public class MovieRepository : IMovieRepository
    {
        //Fields

        // Variable for the DbContext.
        private readonly DbContextMovieCharacterAPI _context;

        // Variable mapper for mapping the DTOs.
        private readonly IMapper _mapper;


        /// <summary>
        /// Default constructor.
        /// </summary>
        public MovieRepository()
        {

        }


        /// <summary>
        /// Constructor for the MovieRepository.
        /// </summary>
        /// <param name="context">Db context</param>
        /// <param name="mapper">Imapper</param>
        public MovieRepository(DbContextMovieCharacterAPI context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        /// <summary>
        /// Get all movies.
        /// </summary>
        /// <returns>Returns a list of movieDto objects</returns>
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMoviesAsync()
        {
            // Assign it to the domain object
            var domainMovies = await _context.Movie.ToListAsync();

            // Map domainMovie with MovieReadDTO
            var dtoMovies = _mapper.Map<List<MovieReadDTO>>(domainMovies);

            return dtoMovies;
        }



        /// <summary>
        /// Get a movie by Id.
        /// </summary>
        /// <param name="id">Id of the movie.</param>
        /// <returns>A movieReadDto object.</returns>
        public async Task<ActionResult<MovieReadDTO>> GetMovieAsync(int id)
        {
            var domainMovie = await _context.Movie.FindAsync(id);

            // Map domainMovie with movieReadDTO
            var movieReadDto = _mapper.Map<MovieReadDTO>(domainMovie);

            return movieReadDto;
        }



        /// <summary>
        /// Get all characters in a movie.
        /// </summary>
        /// <param name="id">Id of the movie.</param>
        /// <returns>A list of characters.</returns>
        public async Task<ActionResult<IEnumerable<Character>>> GetAllCharactersInMovieAsync(int id)
        {
            // Using the domain object and not the Dto.
            var charactersInMovie = await _context.MovieCharacter
                .Where(m => m.MovieId == id)
                .Select(m => m.Character)
                .ToListAsync();

            return charactersInMovie;
        }


        /// <summary>
        /// Create a movie object.
        /// </summary>
        /// <param name="movieDto"></param>
        /// <returns>Returns a movieReaDTO object.</returns>
        public async Task<ActionResult<MovieReadDTO>> PostMovieAsync(MovieCreateDTO movieDto)
        {
              // Map domainMovie with movieCreateDTO
            var domainMovie = _mapper.Map<Movie>(movieDto);

            _context.Movie.Add(domainMovie);
            await _context.SaveChangesAsync();

            // Map movieReadDTO with movie
            MovieReadDTO newMovie = _mapper.Map<MovieReadDTO>(domainMovie);

            return newMovie;
        }

        /// <summary>
        /// Update a movie object.
        /// </summary>
        /// <param name="id">Id of the movie</param>
        /// <param name="movieDto">MovieDTO</param>
        /// <returns>A movieDto object.</returns>
        public async Task<ActionResult<MovieUpdateDTO>> PutMovieAsync(int id, MovieUpdateDTO movieDto)
        {
            // Find the Id.
            var domainMovie = _context.Movie.Find(id);

            domainMovie.Title = movieDto.Title;

            //// Map domainMovie with MovieReadDTO
            var movieUpdateDto = _mapper.Map<MovieUpdateDTO>(domainMovie);

            // Saving the update
            await _context.SaveChangesAsync();

            return movieUpdateDto;
        }


        /// <summary>
        /// Update movies in a franchise.
        /// </summary>
        /// <param name="id">Id of the franchise</param>
        /// <param name="movieId">Id of the movie</param>
        /// <param name="movieDto">MovieDto</param>
        /// <returns>A movieDto object.</returns>
        public async Task<ActionResult<MovieUpdateDTO>> PutMoviesInFranchiseAsync(int id, int movieId, MovieUpdateDTO movieDto)
        {

            var doamainMovieUpdate = await _context.Movie
                 .Where(m => m.MovieId == movieId & m.FranchiseId == id)
                 .FirstAsync();

            doamainMovieUpdate.Title = movieDto.Title;


            //// Map domainMovie with MovieReadDTO
            var movieUpdateDto = _mapper.Map<MovieUpdateDTO>(doamainMovieUpdate);

            // Saving the update
            await _context.SaveChangesAsync();

            return movieUpdateDto;
        }

        /// <summary>
        /// Delete a movie by Id in the database.
        /// </summary>
        /// <param name="id">Id of the movieDto object.</param>
        /// <returns></returns>
        public async Task<ActionResult<MovieDeleteDTO>> DeleteMovieAsync(int id)
        {
            var domainMovie = await _context.Movie.FindAsync(id);

            // Map domainMovie with movieDeleteDTO
            var movieDeleteDto = _mapper.Map<MovieDeleteDTO>(domainMovie);

            _context.Movie.Remove(domainMovie);
            await _context.SaveChangesAsync();

            return movieDeleteDto;
        }

        /// <summary>
        /// Check if the movie exists.
        /// </summary>
        /// <param name="id">Id of movie</param>
        /// <returns>Bool</returns>
        public bool MovieExists(int id)
        {
            return _context.Movie.Any(e => e.MovieId == id);
        }


       
    }
}
