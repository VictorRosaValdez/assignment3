﻿using Assignment3.DTOs.CharacterDTOs;
using Assignment3.Interfaces;
using Assignment3.Models;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3.Dal.Repositories
{
    public class CharacterRepository : ICharacterRepository
    {
        //Fields

        // Variable for the DbContext.
        private readonly DbContextMovieCharacterAPI _context;

        // Variable mapper for mapping the DTOs.
        private readonly IMapper _mapper;


        /// <summary>
        /// Defualt constructor.
        /// </summary>
        public CharacterRepository()
        {
        }

        /// <summary>
        /// Constructor for the CharacterRepository.
        /// </summary>
        /// <param name="context">Db context</param>
        /// <param name="mapper">Imapper</param>

        public CharacterRepository(DbContextMovieCharacterAPI context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        
        /// <summary>
        /// Get all characters.
        /// </summary>
        /// <returns>A list of characterDto objects.</returns>
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharactersAsync()
        {
            // Assign it to the domain object
            var domainCharacters = await _context.Character.ToListAsync();

            // Map domainCharacter with CharacterReadDTO
            var dtoCharacter = _mapper.Map<List<CharacterReadDTO>>(domainCharacters);

            return dtoCharacter;
        }
        /// <summary>
        /// Get a character by Id.
        /// </summary>
        /// <param name="id">Id of the characterDto object.</param>
        /// <returns>A characterDto object.</returns>
        public async Task<ActionResult<CharacterReadDTO>> GetCharacterAsync(int id)
        {
            var domainCharacter = await _context.Character.FindAsync(id);

            // Map domainCharacter with characterReadDTO
            var characterReadDto = _mapper.Map<CharacterReadDTO>(domainCharacter);

            return characterReadDto;
        }

      
        /// <summary>
        /// Create a character object.
        /// </summary>
        /// <param name="characterDto"></param>
        /// <returns>The new characterDto object.</returns>
        public async Task<ActionResult<CharacterReadDTO>> PostCharacterAsync(CharacterCreateDTO characterDto)
        {
            // Map domainCharacter with characterCreateDTO
            var domainCharacter = _mapper.Map<Character>(characterDto);

            _context.Character.Add(domainCharacter);
            await _context.SaveChangesAsync();

            // Map characterReadDTO with character
            CharacterReadDTO newCharacter = _mapper.Map<CharacterReadDTO>(domainCharacter);

            return newCharacter;
        }

        /// <summary>
        /// Update a character object.
        /// </summary>
        /// <param name="id">Id of the character</param>
        /// <param name="characterDto">ChracterDTO</param>
        /// <returns>A characterDto object.</returns>
        public async Task<ActionResult<CharacterUpdateDTO>> PutCharacterAsync(int id, CharacterUpdateDTO characterDto)
        {
         
            // Find the Id.
            var domainCharacter = _context.Character.Find(id);

            domainCharacter.FullName = characterDto.FullName;
         
            //// Map domainCharacter with CharacterReadDTO
            var characterUpdateDto = _mapper.Map<CharacterUpdateDTO>(domainCharacter);
          
            // Saving the update
            await _context.SaveChangesAsync();
            
            return characterUpdateDto;

        }

        /// <summary>
        /// Update characters in a movie.
        /// </summary>
        /// <param name="id">Id of the movie</param>
        /// <param name="characterId">Id of the character</param>
        /// <param name="characterDto">CharacterDto</param>
        /// <returns>A characterDto object.</returns>
        public async Task<ActionResult<CharacterUpdateDTO>> PutCharactersInMovieAsync(int id, int characterId, CharacterUpdateDTO characterDto)
        {
           

            var doamainCharacterUpdate = await (from m in _context.Movie
                                                join mc in _context.MovieCharacter on m.MovieId equals mc.MovieId
                                                join c in _context.Character on mc.CharacterId equals c.CharacterId
                                                where m.MovieId == id & c.CharacterId == characterId
                                                select (mc.Character)).FirstAsync();

            doamainCharacterUpdate.FullName = characterDto.FullName;


            //// Map domainMovie with MovieReadDTO
            var characterUpdateDto = _mapper.Map<CharacterUpdateDTO>(doamainCharacterUpdate);

            // Saving the update
            await _context.SaveChangesAsync();

            return characterUpdateDto;
        }

        /// <summary>
        /// Delete a character by Id.
        /// </summary>
        /// <param name="id">Id of the characterDto object.</param>
        /// <returns></returns>
        public async Task<ActionResult<CharacterDeleteDTO>> DeleteCharacterAsync(int id)
        {
            var domainCharacter = await _context.Character.FindAsync(id);

            // Map domainCharacter with characterDeleteDTO
            var characterDeleteDto = _mapper.Map<CharacterDeleteDTO>(domainCharacter);

            _context.Character.Remove(domainCharacter);
            await _context.SaveChangesAsync();

            return characterDeleteDto;
        }

        /// <summary>
        /// Check if the character exists.
        /// </summary>
        /// <param name="id">Id of character</param>
        /// <returns>Bool</returns>
        public bool CharacterExists(int id)
        {
            return _context.Character.Any(e => e.CharacterId == id);
        }
    }
}
