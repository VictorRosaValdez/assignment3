﻿using Assignment3.DTOs.FranchiseDTOs;
using Assignment3.Interfaces;
using Assignment3.Models;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3.Dal.Repositories
{
    public class FranchiseRepository : IFranchiseRepository
    {
        //Fields

        // Variable for the DbContext.
        private readonly DbContextMovieCharacterAPI _context;

        // Variable mapper for mapping the DTOs.
        private readonly IMapper _mapper;


        /// <summary>
        /// Default constructor.
        /// </summary>
        public FranchiseRepository()
        {

        }

        /// <summary>
        /// Constructor for the FranchiseRepository.
        /// </summary>
        /// <param name="context">Db context</param>
        /// <param name="mapper">Imapper</param>
        public FranchiseRepository(DbContextMovieCharacterAPI context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all franchises.
        /// </summary>
        /// <returns>A list of franchiseDto objects.</returns>
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchisesAsync()
        {
            // Assign it to the domain object
            var domainFranchises = await _context.Franchise.ToListAsync();

            // Map domainFranchise with FranchiseReadDTO
            var dtoFranchise = _mapper.Map<List<FranchiseReadDTO>>(domainFranchises);

            return dtoFranchise;
        }


        /// <summary>
        /// Get a franchise by Id.
        /// </summary>
        /// <param name="id">Id of the franchiseDto object.</param>
        /// <returns>A franchisDto object</returns>
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchiseAsync(int id)
        {
            var domainFranchise = await _context.Franchise.FindAsync(id);

            // Map domainFranchise with franchiseReadDTO
            var franchiseReadDto = _mapper.Map<FranchiseReadDTO>(domainFranchise);

            return franchiseReadDto;
        }

        /// <summary>
        /// Get all the movies in a franchise.
        /// </summary>
        /// <param name="id">Franchise Id</param>
        /// <returns>A list of movies objects.</returns>
        public async Task<ActionResult<Franchise>> GetAllMoviesInFranchiseAsync(int id)
        {
            
            // Using the domain object and not the Dto.
            var moviesInFranchise = await _context.Franchise
                .Include(f => f.Movie)
                .Where(x => x.FranchiseId == id).FirstAsync();

            return moviesInFranchise;
        }

        /// <summary>
        /// Get all the characters in a franchise.
        /// </summary>
        /// <param name="id">The franchise Id</param>
        /// <returns>A list of characters</returns>
        public async Task<ActionResult<IEnumerable<Character>>> GetAllCharactersInFranchiseAsync(int id)
        {
            // Using the domain object and not the Dto.
            var charactersInFranchise = await (from m in _context.Movie 
                                           join mc in _context.MovieCharacter on m.MovieId equals mc.MovieId
                                           join c in _context.Character on mc.CharacterId equals c.CharacterId
                                           where m.FranchiseId == id
                                           select (mc.Character)).ToListAsync();
         
            return charactersInFranchise;
        }

        /// <summary>
        /// Create a franchise object.
        /// </summary>
        /// <param name="franchiseDto"></param>
        /// <returns>The new franchiseDto object.</returns>
        public async Task<ActionResult<FranchiseReadDTO>> PostFranchiseAsync(FranchiseCreateDTO franchiseDto)
        {
            // Map domainFranchise with FranchiseCreateDTO
            var domainFranchise = _mapper.Map<Franchise>(franchiseDto);

            _context.Franchise.Add(domainFranchise);
            await _context.SaveChangesAsync();

            // Map FranchiseReadDTO with franchise
            FranchiseReadDTO newFranchise = _mapper.Map<FranchiseReadDTO>(domainFranchise);

            return newFranchise;
        }

        

        /// <summary>
        /// Update a franchise object.
        /// </summary>
        /// <param name="id">Id of the franchise</param>
        /// <param name="franchiseDto">FranchiseDTO</param>
        /// <returns>A franchiseDto object.</returns>
        public async Task<ActionResult<FranchiseUpdateDTO>> PutFranchiseAsync(int id, FranchiseUpdateDTO franchiseDto)
        {
            // Find the Id.
            var domainFranchise = _context.Franchise.Find(id);

            domainFranchise.Name = franchiseDto.Name;

            //// Map domainFranchise with franchiseReadDTO
            var franchiseUpdateDto = _mapper.Map<FranchiseUpdateDTO>(domainFranchise);

            // Saving the update
            await _context.SaveChangesAsync();

            return franchiseUpdateDto;
        }

        /// <summary>
        /// Delete a franchise by Id in the database.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult<FranchiseDeleteDTO>> DeleteFranchiseAsync(int id)
        {
            var domainFranchise = await _context.Franchise.FindAsync(id);

            // Map domainFranchise with franchiseDeleteDTO
            var franchiseDeleteDto = _mapper.Map<FranchiseDeleteDTO>(domainFranchise);

            _context.Franchise.Remove(domainFranchise);
            await _context.SaveChangesAsync();

            return franchiseDeleteDto;
        }

        /// <summary>
        /// Check if the franchise exists.
        /// </summary>
        /// <param name="id">Id of franchise</param>
        /// <returns>Bool</returns>
        public bool FranchiseExists(int id)
        {
            return _context.Franchise.Any(e => e.FranchiseId == id);
        }

    }
}
