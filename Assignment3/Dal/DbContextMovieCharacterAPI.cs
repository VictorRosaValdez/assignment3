﻿using Microsoft.EntityFrameworkCore;

namespace Assignment3.Models
{
    public class DbContextMovieCharacterAPI : DbContext
    {
    
        // Properties for creating the tables in the SSMS with the EF.
       
        // Table of Movie
        public DbSet<Movie> Movie { get; set; }

        // Table of Character
        public DbSet<Character> Character { get; set; }
        
        // Table of Franchise
        public DbSet<Franchise> Franchise { get; set; }

        // Intermediate table MovieCharacter
        public DbSet<MovieCharacter> MovieCharacter { get; set; }

        /// <summary>
        ///  Constructor for the use of the connection string in the Startup.cs
        /// </summary>
        /// <param name="options"></param>
        public DbContextMovieCharacterAPI(DbContextOptions options) : base(options)
        {

        }
        /// <summary>
        /// This method is to create realtions and insert seeded data in the database with EF.
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Making the relation with the foreign keys
            modelBuilder.Entity<MovieCharacter>().HasKey(mgg => new {mgg.MovieId, mgg.CharacterId});

            //seeded data for franchise
            modelBuilder.Entity<Franchise>().HasData(SeedHelper.GetFranchiseSeeds());

            // Seeded data for characters
            modelBuilder.Entity<Character>().HasData(SeedHelper.GetCharacterSeeds());

            // Seeded data for movies
            modelBuilder.Entity<Movie>().HasData(SeedHelper.GetMovieSeeds());

            // Seeded data for movies
            modelBuilder.Entity<MovieCharacter>().HasData(SeedHelper.GetMovieCharacterSeeds());

        }
    }
}
