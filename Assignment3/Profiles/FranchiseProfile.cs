﻿using Assignment3.DTOs.FranchiseDTOs;
using Assignment3.Models;
using AutoMapper;


namespace Assignment3.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            // Mapping from the domain object to the readDTO object.
            CreateMap<Franchise, FranchiseReadDTO>();

            // Mapping from the createDTO object to the domain object. ReverseMap is for navigating both ways.
            CreateMap<FranchiseCreateDTO, Franchise>().ReverseMap();

            // Mapping from the updateDTO object to the domain object. ReverseMap is for navigating both ways.
            CreateMap<FranchiseUpdateDTO, Franchise>().ReverseMap();

            // Mapping from the domain object to the deleteDTO object.
            CreateMap<Franchise, FranchiseDeleteDTO>();
        }
    }
}
