﻿using Assignment3.DTOs.CharacterDTOs;
using Assignment3.Models;
using AutoMapper;

namespace Assignment3.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            // Mapping from the domain object to the readDTO object.
            CreateMap<Character, CharacterReadDTO>();

            // Mapping from the createDTO object to the domain object. ReverseMap is for navigating both ways.
            CreateMap<CharacterCreateDTO, Character>().ReverseMap();

            // Mapping from the createDTO object to the domain object. ReverseMap is for navigating both ways.
            CreateMap<CharacterUpdateDTO, Character>().ReverseMap();

            // Mapping from the domain object to the deleteDTO object.
            CreateMap<Character, CharacterDeleteDTO>();
        }
    }
}
