﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment3.Models;
using System.Net.Mime;
using AutoMapper;
using Assignment3.DTOs.CharacterDTOs;
using Assignment3.Interfaces;
using Assignment3.Dal.Repositories;

namespace Assignment3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class CharactersController : ControllerBase
    {

        // Private field
        private readonly ICharacterRepository _character;

        /// <summary>
        /// Constructor of the character.
        /// </summary>
        /// <param name="character"></param>
        public CharactersController(ICharacterRepository character)
        {

            _character = character;
            
        }

        // GET: api/Characters
        /// <summary>
        /// Get all characters.
        /// </summary>
        /// <response code="200">Succesfully returns a character object.</response>
        /// <returns>A list of characterDto objects.</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters()
        {

            return await _character.GetCharactersAsync();

        }

        // GET: api/Characters/5
        /// <summary>
        /// Get a character from the database by ID.
        /// </summary>
        /// <param name="id">ID of the character you want to get.</param>
        /// <returns>A characterDto object</returns>
        /// <response code="200">Succesfully returns a character object.</response>
        /// <response code="400">Error: Response with this is a bad request.</response>
        /// <response code="404">Error: The object you are looking for is not found.</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int id)
        {
            var character = await _character.GetCharacterAsync(id);

            if (character.Value == null)
            {
                return NotFound();
            }

            return character;
        }

    

        // PUT: api/Characters/5
        /// <summary>
        /// Update a character.
        /// </summary>
        /// <param name="id">Id of the character</param>
        /// <param name="characterDto">CharacterDto</param>
        /// <returns></returns>
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpPut("{id}")]
        public async Task<ActionResult<CharacterUpdateDTO>> PutCharacter(int id, CharacterUpdateDTO characterDto)
        {

            CharacterRepository character = new ();


            try
            {
                await _character.PutCharacterAsync(id, characterDto);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!character.CharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }


            return NoContent();
        }

        // PUT: api/Characters/movie
        /// <summary>
        /// Update characters in a movie.
        /// </summary>
        /// <param name="id">Movie Id</param>
        /// <param name="characterId">Character Id</param>
        /// <param name="characterDto"></param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpPut("{id}/movie")]
        public async Task<ActionResult<CharacterUpdateDTO>> PutCharactersInMovie(int id, int characterId, CharacterUpdateDTO characterDto)
        {
            await _character.PutCharactersInMovieAsync(id, characterId, characterDto);

            return NoContent();


        }

            // POST: api/Characters
            /// <summary>
            /// Create a character object.
            /// </summary>
            /// <param name="characterDto"></param>
            /// <returns>The created charactereDto object.</returns>
            /// <response code="201">Succesfully object created.</response>
            // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
            [ProducesResponseType(StatusCodes.Status201Created)]
        [HttpPost]
        public async Task<ActionResult<CharacterReadDTO>> PostCharacter(CharacterCreateDTO characterDto)
        {

            // New character object.
            var character = await _character.PostCharacterAsync(characterDto);

            // Get Movie Id for new movie object.
            int characterId = _character.PostCharacterAsync(characterDto).Id;

            // It works
            return CreatedAtAction("GetCharacter", new { id = characterId }, character);
        }

        // DELETE: api/Characters/5
        /// <summary>
        /// Delete an object by Id in the database.
        /// </summary>
        /// <param name="id">Id of the object.</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpDelete("{id}")]
        public async Task<ActionResult<CharacterDeleteDTO>> DeleteCharacter(int id)
        {
            var character = await _character.DeleteCharacterAsync(id);
            if (character == null)
            {
                return NotFound();
            }


            return NoContent();
        }

    
    }
}
