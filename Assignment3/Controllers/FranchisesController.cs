﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment3.Models;
using System.Net.Mime;
using AutoMapper;
using Assignment3.DTOs.FranchiseDTOs;
using Assignment3.Interfaces;
using Assignment3.Dal.Repositories;

namespace Assignment3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class FranchisesController : ControllerBase
    {
        // Private field
        private readonly IFranchiseRepository _franchise;

        /// <summary>
        /// Constructor of FranchiseController.
        /// </summary>
        /// <param name="franchise"></param>
        public FranchisesController(IFranchiseRepository franchise)
        {
            _franchise = franchise;
        }

        // GET: api/Franchises
        /// <summary>
        /// Get all franchises.
        /// </summary>
        /// <response code="200">Succesfully returns a character object.</response>
        /// <returns>A list of franchiseDto objects.</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            return await _franchise.GetFranchisesAsync();
        }

        // GET: api/Franchises/5

        /// <summary>
        ///  Get a Franchise from the database by ID.
        /// </summary>
        /// <param name="id">ID of the franchise you want to get.</param>
        /// <returns>A franchiseDto object.</returns>
        /// <response code="200">Succesfully returns a franchise object.</response>
        /// <response code="400">Error: Response with this is a bad request.</response>
        /// <response code="404">Error: The object you are looking for is not found.</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            var franchise = await _franchise.GetFranchiseAsync(id);

            if (franchise.Value == null)
            {
                return NotFound();
            }

            return franchise;
        }
        // GET: api/Franchises/Allmovies.

        /// <summary>
        /// Get all the movies in a franchise.
        /// </summary>
        /// <param name="id">The Id of the franchise.</param>
        /// <returns>A list of movie objects.</returns>
        /// <response code="200">Succesfully returns the movie objects.</response>
        /// <response code="400">Error: Response with this is a bad request.</response>
        /// <response code="404">Error: The object you are looking for is not found.</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}/franchise")]
        public async Task<ActionResult<Franchise>> GetAllMoviesInFranchise(int id)
        {

            var movieInfrachise = await _franchise.GetAllMoviesInFranchiseAsync(id);

            if (movieInfrachise.Value == null)
            {
                return NotFound();
            }

            return Ok(movieInfrachise);
        }

     

        /// <summary>
        /// Get all the character in a franchise.
        /// </summary>
        /// <param name="id">The Id of the franchise.</param>
        /// <returns>A list of characters objects.</returns>
        /// <response code="200">Succesfully returns the character objects.</response>
        /// <response code="400">Error: Response with this is a bad request.</response>
        /// <response code="404">Error: The object you are looking for is not found.</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}/movie/character")]
        public async Task<ActionResult<Franchise>> GetAllCharactersInFranchise(int id)
        {

            var charactersInfrachise = await _franchise.GetAllCharactersInFranchiseAsync(id);

            if (charactersInfrachise.Value == null)
            {
                return NotFound();
            }

            return Ok(charactersInfrachise);
        }

        // PUT: api/Franchises/5
        /// <summary>
        /// Update a franchise.
        /// </summary>
        /// <param name="id">Id of the franchise</param>
        /// <param name="franchiseDto">FranchiseDto</param>
        /// <returns></returns>
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpPut("{id}")]
         public async Task<ActionResult<FranchiseUpdateDTO>> PutFranchise(int id, FranchiseUpdateDTO franchiseDto)
        {

            FranchiseRepository franchise = new ();


            try
            {
                await _franchise.PutFranchiseAsync(id, franchiseDto);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!franchise.FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }


            return NoContent();
        }

        // POST: api/Franchises
        /// <summary>
        /// Create a franchise object.
        /// </summary>
        /// <param name="franchiseDto"></param>
        /// <response code="201">Succesfully object created.</response>
        /// <returns>The created franchise object.</returns>
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [ProducesResponseType(StatusCodes.Status201Created)]
        [HttpPost]
        public async Task<ActionResult<FranchiseReadDTO>> PostFranchise(FranchiseCreateDTO franchiseDto)
        {
            // New franchise object.
            var franchise = await _franchise.PostFranchiseAsync(franchiseDto);

            // Get franchise Id for new franchise object.
            int franchiseId = _franchise.PostFranchiseAsync(franchiseDto).Id;

            return CreatedAtAction("GetFranchise", new { id = franchiseId }, franchise);
        }

        // DELETE: api/Franchises/5
        /// <summary>
        /// Delete a franchise object by Id in the database.
        /// </summary>
        /// <param name="id">Id of the franchise object.</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            var franchise = await _franchise.DeleteFranchiseAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }

            return NoContent();
        }

     
    }
}
