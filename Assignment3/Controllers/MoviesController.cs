﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment3.Models;
using AutoMapper;
using Assignment3.DTOs.MovieDTOs;
using System.Net.Mime;
using Assignment3.Interfaces;
using Assignment3.Dal.Repositories;

namespace Assignment3.Controllers
{
   
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class MoviesController : ControllerBase
    {
        // Private field
        private readonly IMovieRepository _movie;


        /// <summary>
        /// Constructor of MoviesCrontroller.
        /// </summary>
        /// <param name="movie"></param>
        public MoviesController(IMovieRepository movie)
        {
            _movie = movie;
        }


        // GET: api/Movies
        /// <summary>
        /// Get all movies.
        /// </summary>
        /// <response code="200">Succesfully returns a movie object.</response>
        /// <returns>A list of movieDto objects.</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
        {
            // It works!
            return await _movie.GetMoviesAsync();
            
        }

        // GET: api/Movies/5
        /// <summary>
        /// Get a movie from the database by ID.
        /// </summary>
        /// <param name="id">ID of the movie you want to get.</param>
        /// <returns>A movieDto object.</returns>
        /// <response code="200">Succesfully returns a movie object.</response>
        /// <response code="400">Error: Response with this is a bad request.</response>
        /// <response code="404">Error: The object you are looking for is not found.</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
        {
            var movie = await _movie.GetMovieAsync(id);

            if ( movie.Value== null)
            {
                return NotFound();
            }

            return movie;
        }

        /// <summary>
        /// Get all characters in a movie.
        /// </summary>
        /// <param name="id">Movie Id</param>
        /// <returns>A list of character objects.</returns>
        /// <response code="200">Succesfully returns the character objects.</response>
        /// <response code="400">Error: Response with this is a bad request.</response>
        /// <response code="404">Error: The object you are looking for is not found.</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}/movie/character")]
        public async Task<ActionResult<Movie>> GetAllCharacterInMovie(int id)
        {
            var charactersInMovie = await _movie.GetAllCharactersInMovieAsync(id);

            if (charactersInMovie.Value == null)
            {
                return NotFound();
            }

            
            return Ok(charactersInMovie);
        }



        /// <summary>
        /// Update a movie.
        /// </summary>
        /// <param name="id">Id of movie</param>
        /// <param name="movieDto">MovieDto</param>
        /// <returns></returns>
        // PUT: api/Movies/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpPut("{id}")]
        public async Task<ActionResult<MovieUpdateDTO>> PutMovie(int id, MovieUpdateDTO movieDto)
        {
            MovieRepository franchise = new();


            try
            {
                await _movie.PutMovieAsync(id, movieDto);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!franchise.MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // PUT: api/Movies/franchise
        /// <summary>
        /// Update movies in a franchise.
        /// </summary>
        /// <param name="id">Franchise Id</param>
        /// <param name="movieId"></param>
        /// <param name="movieDto"></param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpPut("{id}/franchise")]
        public async Task<ActionResult<MovieUpdateDTO>> PutMoviesInFranchise(int id, int movieId, MovieUpdateDTO movieDto)
        {
            await _movie.PutMoviesInFranchiseAsync(id,movieId, movieDto);

            return NoContent();
        }



        // POST: api/Movies
        /// <summary>
        /// Create a movie object.
        /// </summary>
        /// <param name="movieDto"></param>
        /// <returns>The created movieDto object.</returns>
        /// <response code="201">Succesfully object created.</response>
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [ProducesResponseType(StatusCodes.Status201Created)]
        [HttpPost]
        public async Task<ActionResult<MovieReadDTO>> PostMovie(MovieCreateDTO movieDto)
        {
            // New movie object.
            var movie = await _movie.PostMovieAsync(movieDto);

            // Get Movie Id for new movie object.
            int movieId = _movie.PostMovieAsync(movieDto).Id;

           
            return CreatedAtAction("GetMovie", new { id = movieId }, movie);
        }

        // DELETE: api/Movies/5
        /// <summary>
        /// Delete a movie by Id.
        /// </summary>
        /// <param name="id">Movie Id</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpDelete("{id}")]
        public async Task<ActionResult<MovieDeleteDTO>> DeleteMovie(int id)
        {
            var movie = await _movie.DeleteMovieAsync(id);

            if (movie == null)
            {
                return NotFound();
            }

           
           
            return NoContent();
        }

    }
}
