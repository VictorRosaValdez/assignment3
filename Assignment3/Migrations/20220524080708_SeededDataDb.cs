﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Assignment3.Migrations
{
    public partial class SeededDataDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Character",
                columns: new[] { "CharacterId", "Alias", "FullName", "Gender", "Picture" },
                values: new object[,]
                {
                    { 1, "Tony Stark / Iron Man", "Robert Downey Jr.", "Male", "https://en.wikipedia.org/wiki/Avengers:_Endgame#/media/File:Robert_Downey_Jr_2014_Comic_Con_(cropped).jpg" },
                    { 2, "General Maximus Decimus Meridius", "Russell Crowe", "Male", "https://en.wikipedia.org/wiki/Russell_Crowe#/media/File:Russell_Crowe_(33994020424).jpg" },
                    { 3, "Jake Sully", "Sam Worthington", "Male", "https://en.wikipedia.org/wiki/Sam_Worthington#/media/File:Sam_Worthington_2013.jpg" },
                    { 4, "Natasha Romanoff / Black Widow", "Scarlett Johansson", "Female", "https://en.wikipedia.org/wiki/Avengers:_Endgame#/media/File:Scarlett_Johansson_by_Gage_Skidmore_2_(cropped).jpg" }
                });

            migrationBuilder.InsertData(
                table: "Franchise",
                columns: new[] { "FranchiseId", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "Marvel Studios, LLC[4] (originally known as Marvel Films from 1993 to 1996)", "Marvel Studios" },
                    { 2, "DreamWorks Pictures (also known as DreamWorks SKG and formerly DreamWorks Studios", "DreamWorks Pictures" },
                    { 3, "20th Century Studios, Inc., (formerly known as 20th Century-Fox (1935–1985)", "20th Century Studios" }
                });

            migrationBuilder.InsertData(
                table: "Movie",
                columns: new[] { "MovieId", "Director", "FranchiseId", "Genre", "Picture", "Title", "Trailer", "Year" },
                values: new object[,]
                {
                    { 1, "Anthony Russo", 1, "Fantasy film", "https://en.wikipedia.org/wiki/The_Avengers_(2012_film)#/media/File:The_Avengers_(2012_film)_poster.jpg", "Avengers", "https://www.youtube.com/watch?v=eOrNdBpGMv8", 2012 },
                    { 2, "Anthony Russo", 1, "Fantasy film", "https://en.wikipedia.org/wiki/Avengers:_Endgame#/media/File:Avengers_Endgame_poster.jpg", "Avengers: Endgame", "https://www.youtube.com/watch?v=TcMBFSGVi1c", 2019 },
                    { 3, "Ridley Scott", 2, "Historical drama", "https://en.wikipedia.org/wiki/Gladiator_(2000_film)#/media/File:Gladiator_(2000_film_poster).png", "Gladiator", "https://www.youtube.com/watch?v=owK1qxDselE", 2000 },
                    { 4, "James Cameron", 3, "Science fiction film", "https://en.wikipedia.org/wiki/Avatar_(2009_film)#/media/File:Avatar_(2009_film)_poster.jpg", "Avatar", "https://www.youtube.com/watch?v=a8Gx8wiNbs8", 2000 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Character",
                keyColumn: "CharacterId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Character",
                keyColumn: "CharacterId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Character",
                keyColumn: "CharacterId",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Character",
                keyColumn: "CharacterId",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Movie",
                keyColumn: "MovieId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Movie",
                keyColumn: "MovieId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Movie",
                keyColumn: "MovieId",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Movie",
                keyColumn: "MovieId",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Franchise",
                keyColumn: "FranchiseId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Franchise",
                keyColumn: "FranchiseId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Franchise",
                keyColumn: "FranchiseId",
                keyValue: 3);
        }
    }
}
